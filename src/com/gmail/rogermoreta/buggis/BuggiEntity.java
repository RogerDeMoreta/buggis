package com.gmail.rogermoreta.buggis;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Roger on 29/03/2016.
 */
@Entity
@Table(name = "buggi", schema = "buggis", catalog = "")
public class BuggiEntity implements Serializable {
    private int id;
    private String matricula;
    private int metros;
    private short estado;

    @Id
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Column(name = "matricula", length=1024)
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }


    @Column(name = "metros", length=1024)
    public int getMeters() {
        return metros;
    }

    public void setMeters(int metros) {
        this.metros = metros;
    }


    @Column(name = "disponible")
    public short getEstado() {
        return estado;
    }

    public void setEstado(short estado) {
        this.estado = estado;
    }

}

package com.gmail.rogermoreta.buggis;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Roger on 29/03/2016.
 */
@Entity
@Table(name = "incidencia", schema = "buggis", catalog = "")
public class IncidenciaEntity implements Serializable {
    private String id;

    @Id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

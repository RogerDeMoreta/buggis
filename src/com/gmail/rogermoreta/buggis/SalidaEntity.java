package com.gmail.rogermoreta.buggis;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Roger on 29/03/2016.
 */
@Entity
@Table(name = "sortida", schema = "buggis")
public class SalidaEntity implements Serializable{
    private Integer id;
    private String name;
    private String matricula;
    private boolean seHaRotoEnLaSalida;
    private String descripcion;
    private Timestamp fechaSalida;
    private Timestamp creationDate;

    @Id @GeneratedValue
    @Column(name = "id", unique = true)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(updatable = false, name = "name", length=1024)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(updatable = false, name = "matricula", length=1024)
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }


    @Column(name = "roto_en_salida")
    public boolean getseHaRotoEnLaSalida() {
        return seHaRotoEnLaSalida;
    }

    public void setseHaRotoEnLaSalida(boolean seHaRotoEnLaSalida) {
        this.seHaRotoEnLaSalida = seHaRotoEnLaSalida;
    }

    @Column(name = "descripcion", length=1024)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    @Column(name = "fecha_salida")
    public Timestamp getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Timestamp fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    @Column(updatable = false, name = "creation_time")
    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }
}

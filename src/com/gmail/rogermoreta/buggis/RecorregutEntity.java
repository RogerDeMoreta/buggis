package com.gmail.rogermoreta.buggis;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Roger on 29/03/2016.
 */
@Entity
@Table(name = "recorregut", schema = "buggis", catalog = "")
public class RecorregutEntity implements Serializable {
    private String id;
    private int metros;
    private String nom;

    @Id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "metros", length=1024)
    public int getMeters() {
        return metros;
    }

    public void setMeters(int metros) {
        this.metros = metros;
    }

    @Column(name = "nom", length=1024)
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}

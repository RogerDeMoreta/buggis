package com.gmail.rogermoreta.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Roger on 03/04/2016.
 */

public class LoginController extends HttpServlet {

    SessionController sc;

    public LoginController() {
        sc = SessionController.getInstance();
    }

    private void processPetition(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession(true);
        String username = request.getParameter("name");
        String password = request.getParameter("password");
        if (username != null && password != null) { //Tenemos datos en el request para hacer un login

            response.setContentType("text/plain");  // Set content type of the response so that jQuery knows what it can expect.
            response.setCharacterEncoding("UTF-8"); // You want world domination, huh?
            int nivelPrivilegios = usuarioPrivilegios(username,password);
            session.setAttribute("tipo", nivelPrivilegios);
            response.getWriter().write(nivelPrivilegios+"");       // Write response body.
            if (nivelPrivilegios>0) {
                session.setAttribute("userInside",true);
                session.setAttribute("userName",username);
            }
        }
        else { // No hay datos en el request
            Boolean usuarioIniciado = (Boolean) session.getAttribute("userInside") ;
            if (usuarioIniciado != null && usuarioIniciado) { //Usuario identificado
                response.sendRedirect("./introRecorrido.jsp");
            }
            else { // No esta identificado
                response.sendRedirect("./login.jsp");
            }
        }

    }

    private int usuarioPrivilegios(String username, String password) {
        return sc.verificaUsuario(username, password);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processPetition(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processPetition(request, response);
    }

}

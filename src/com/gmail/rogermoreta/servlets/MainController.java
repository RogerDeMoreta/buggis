package com.gmail.rogermoreta.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Roger on 03/04/2016.
 */

public class MainController extends HttpServlet {

    private void processPetition(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect("./login.jsp");
    }


    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processPetition(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processPetition(request, response);
    }

}

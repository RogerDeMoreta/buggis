package com.gmail.rogermoreta.servlets;

import com.gmail.rogermoreta.buggis.BuggiEntity;
import com.gmail.rogermoreta.buggis.SalidaEntity;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Roger on 03/04/2016.
 */

public class SalidasController extends HttpServlet {

    SessionController sc;
    HttpSession session;

    public SalidasController() {
        sc = SessionController.getInstance();
    }

    private void processPetition(HttpServletRequest request, HttpServletResponse response) throws IOException {
        session = request.getSession(true);
        String username = request.getParameter("name");
        String password = request.getParameter("password");
        Boolean usuarioIniciado = (Boolean) session.getAttribute("userInside") ;
        if (usuarioIniciado != null && usuarioIniciado) { //Usuario identificado
            //Intentamos coger datos del post o get (del request)
            String rutaSelected = request.getParameter("rutaSelected");
            String buggies[] = request.getParameterValues("buggies[]");
            String tiempoSalida = request.getParameter("tiempoSalida");
            String comentario = request.getParameter("comentario");

            if (rutaSelected != null || buggies != null || tiempoSalida!=null || comentario!=null) { //Intentan añadir una nueva salida, mediante un post ajax (sino, malo!)
                if (sc.createNewSalida(rutaSelected,buggies,tiempoSalida,comentario)) { //La nueva salida se ha introducido correctamente, mandamos un 1 de respuesta a ajax
                    actualizarDatosDePaginaConBD();
                    response.setContentType("text/plain");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().write("1");
                }
                else { //Ha habido algun error insertando la nueva salida, mandamos un 0 de respuesta a ajax
                    response.setContentType("text/plain");
                    response.setCharacterEncoding("UTF-8");
                    response.getWriter().write("0");
                }
            }
            else { //Venimos de un inicio de sesion
                actualizarDatosDePaginaConBD();
                response.sendRedirect("./introRecorrido.jsp");
            }
        }
        else { // No esta identificado
            response.sendRedirect("./login.jsp");
        }

    }

    private void actualizarDatosDePaginaConBD() {
        ArrayList<BuggiEntity> buggiesDB = sc.getBuggies();
        session.setAttribute("numBuggies",buggiesDB.size());
        session.setAttribute("buggies",buggiesDB);

        ArrayList<SalidaEntity> salidas = sc.getSalidas();
        session.setAttribute("numSalidas", salidas.size());
        session.setAttribute("salidas", salidas);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processPetition(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processPetition(request, response);
    }

}

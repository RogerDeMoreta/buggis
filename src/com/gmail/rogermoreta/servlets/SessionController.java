package com.gmail.rogermoreta.servlets;

import com.gmail.rogermoreta.buggis.BuggiEntity;
import com.gmail.rogermoreta.buggis.RecorregutEntity;
import com.gmail.rogermoreta.buggis.SalidaEntity;
import com.gmail.rogermoreta.buggis.UserEntity;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Roger on 06/04/2016.
 */
public class SessionController {

    private static final SessionFactory ourSessionFactory;
    private static final ServiceRegistry serviceRegistry;
    private static SessionController instance;
    private ArrayList<UserEntity> listaUsuarios = null;
    private ArrayList<SalidaEntity> listaSalidas = null;
    private ArrayList<RecorregutEntity> listaRecorreguts = null;
    private ArrayList<BuggiEntity> listaBuggies = null;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            ourSessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionController getInstance() {
        if (instance == null) {
            instance = new SessionController();
        }
        return instance;
    }

    private SessionController() {
        setUsers();
    }

    private void setUsers() {
        if (listaUsuarios == null) {
            listaUsuarios = new ArrayList<>();
            final Session session = getSession();
            session.beginTransaction();
            try {
                Query q = session.createQuery("from UserEntity");
                List listaExistentes = q.list();
                for (Object ue : listaExistentes) {
                    listaUsuarios.add((UserEntity) ue);
                }
            } finally {
                session.close();
            }
        }
    }

    private static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }



    public boolean createNewUser(String name, String password, String email) {
        boolean created = false;
        final Session session = getSession();
        session.beginTransaction();
        try {
            UserEntity ue = new UserEntity();
            ue.setName(name);
            ue.setEmail(email);
            ue.setPassword(password);
            Query q = session.createQuery("from UserEntity as ue where ue.name=:name or ue.email=:email");
            q.setProperties(ue);
            List listaExistentes = q.list();
            if (listaExistentes.size() == 0) {
                session.save(ue);
                session.getTransaction().commit();
                session.flush();
                listaUsuarios.add(ue);
                created = true;
            }
        } finally {
            session.close();
        }
        return created;
    }

    public boolean createNewRecorregut(String name, int meters) {
        boolean created = false;
        final Session session = getSession();
        session.beginTransaction();
        try {
            RecorregutEntity newRecorregut = new RecorregutEntity();
            newRecorregut.setNom(name);
            newRecorregut.setMeters(meters);
            session.save(newRecorregut);
            session.getTransaction().commit();
            session.flush();
            listaRecorreguts.add(newRecorregut);
            created = true;
        } finally {
            session.close();
        }
        return created;
    }

    public Long contarElementosDeLaTalbla(String tabla) {
        Long result = 0l;
        final Session session = getSession();
        try {
            result = (Long) session.createQuery("select count(*) from "+tabla).iterate().next();
        } finally {
            session.close();
        }
        return result;
    }

    public int verificaUsuarioDB(String name, String password) {
        int tipo = 0;
        final Session session = getSession();
        session.beginTransaction();
        try {
            UserEntity ue = new UserEntity();
            ue.setName(name);
            ue.setPassword(password);
            Query q = session.createQuery("from UserEntity as ue where ue.name=:name and ue.password=:password");
            q.setProperties(ue);
            List listaExistentes = q.list();
            if (listaExistentes.size() > 0) {
                tipo = 1;
            }
        } finally {
            session.close();
        }
        return tipo;
    }

    public int verificaUsuario(String name, String password) {
        int tipo = 0;
        if (name != null && password!=null) {
            for (UserEntity ue : listaUsuarios) {
                if (name.equals(ue.getName()) && password.equals(ue.getPassword())) tipo = 1;
            }
        }
        return tipo;
    }

    public ArrayList<SalidaEntity> getSalidas() {
        final Session session = getSession();
        session.beginTransaction();
        try {
            Query q = session.createQuery("from SalidaEntity group by fechaSalida");
            List listaExistentes = q.list();
            listaSalidas = new ArrayList<>(listaExistentes);
        } finally {
            session.close();
        }
        return listaSalidas;
    }

    public ArrayList<BuggiEntity> getBuggies() {
        final Session session = getSession();
        session.beginTransaction();
        try {
            Query q = session.createQuery("from BuggiEntity ");
            List listaExistentes = q.list();
            listaBuggies = new ArrayList<>(listaExistentes);
        } finally {
            session.close();
        }
        return listaBuggies;
    }

    public ArrayList<RecorregutEntity> getRecorreguts() {
        final Session session = getSession();
        session.beginTransaction();
        try {
            Query q = session.createQuery("from RecorregutEntity ");
            List listaExistentes = q.list();
            listaRecorreguts = new ArrayList<>(listaExistentes);
        } finally {
            session.close();
        }
        return listaRecorreguts;
    }

    public boolean createNewSalida(String rutaSelected, String[] buggies, String tiempoSalida, String comentario) {
        boolean created = false;
        if (rutaSelected != null && !"".equals(rutaSelected) && buggies != null && tiempoSalida != null && !"".equals(tiempoSalida)) {
            final Session session = getSession();
            session.beginTransaction();
            try {
                for (int i = 0; i < buggies.length; i++) {
                    SalidaEntity salida = new SalidaEntity();
                    salida.setName(rutaSelected);
                    salida.setDescripcion(comentario);
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm");
                    Date parsedDate = dateFormat.parse(tiempoSalida);
                    Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
                    salida.setFechaSalida(timestamp);
                    salida.setseHaRotoEnLaSalida(false);
                    salida.setMatricula(buggies[i]);
                    session.save(salida);
                    listaSalidas.add(salida);
                }
                session.getTransaction().commit();
                session.flush();
                created = true;
            } catch (ParseException e) {
                e.printStackTrace();
            } finally {
                session.close();
            }
        }
        return created;
    }
}

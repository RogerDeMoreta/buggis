<%--
  Created by IntelliJ IDEA.
  User: Roger
  Date: 03/04/2016
  Time: 22:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Inicio de sesión</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/theme.css" rel="stylesheet">

        <!-- icon -->
        <link rel="shortcut icon" href="img/buggy16.ico" />

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body role="document">
        <!-- Fixed navbar -->
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="pull-left">
                        <img src="img/buggy512.png" style="height:50px; margin-left: 10px; margin-right: 15px">
                    </a>
                    <!--<a class="navbar-brand">Buggies</a>-->
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a>Inicio sesión</a></li>
                        <li>               <a href="register.jsp">Registro</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
        <div class="container theme-showcase" role="main">
            <!-- Main jumbotron for a primary marketing message or call to action -->
            <div class="jumbotron">
                <h1>Iniciar sesión</h1>
                <p>Inicia sesión para poder acceder a la página de Buggies</p>
            </div>
            <h3 class="hide" id="identificationError" style="color:darkred">Error en el usuario o contraseña</h3>
            <div class="form-horizontal" data-toggle="validator" role="form">
                <div class="row form-group has-feedback">
                    <label for="inputName" class="control-label">Usuario</label>
                    <input type="text" class="form-control" id="inputName" name="name" placeholder="Cliente" required>
                    <div class="help-block with-errors"></div>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                </div>
                <div class="row form-group has-feedback">
                    <label class="control-label" for="inputPassword">Contraseña</label>
                    <input type="password" data-minlength="6" class="form-control" id="inputPassword" name="password" data-error="Mínimo 6 caracteres" placeholder="Contraseña" required>
                    <div class="help-block with-errors"></div>
                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                </div>
                <div class="row form-group">
                    <button autofocus type="submit" id="botonSubmit" class="btn btn-primary">Entrar!</button>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-1.12.2.min.js"></script>
        <script src="js/validator.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script>
            $("#inputName").keyup(function (e) {
                if (e.keyCode == 13) {
                    $("#inputPassword").focus();
                }
            });

            $("#inputPassword").keyup(function (e) {
                if (e.keyCode == 13) {
                    $("#botonSubmit").focus();
                }
            });

            $(document).ready(function() {
                $('#botonSubmit').click(function(event) {
                    var nombreVar = $('#inputName').val();
                    var passwordVar = $('#inputPassword').val();
                    // Si en vez de por post lo queremos hacer por get, cambiamos el $.post por $.get
                    $.post('logincontroller', {
                        name: nombreVar,
                        password: passwordVar
                    },
                    function(responseText) {
                        $("#pleaseWaitDialog").remove();
                        $(".modal-backdrop").remove();
                        if (responseText == "0") {
                            $('#identificationError').removeClass("hide");
                        }
                        else {
                            window.location.href = "salidascontroller";
                        }
                    });
                    ($(
                    '<div class="modal fade" id="pleaseWaitDialog" role="dialog" data-keyboard="false">' +
                        '<div class="modal-dialog">' +
                            '<!-- Modal content-->' +
                            '<div class="modal-content">' +
                                '<div class="modal-header">' +
                                    '<h1>Processing...</h1>' +
                                '</div>' +
                                '<div class="modal-body">' +
                                    '<div class="progress">' +
                                        '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
                                            '<span class="sr-only">100% Complete</span>' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>'
                    )).modal();
                });
            });
        </script>
    </body>
</html>

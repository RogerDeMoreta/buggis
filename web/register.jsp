<%--
  Created by IntelliJ IDEA.
  User: Roger
  Date: 03/04/2016
  Time: 22:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Registro</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/theme.css" rel="stylesheet">

        <!-- icon -->
        <link rel="shortcut icon" href="img/buggy16.ico" />

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body role="document">
    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="pull-left">
                    <img src="img/buggy512.png" style="height:50px; margin-left: 10px; margin-right: 15px">
                </a>
                <!--<a class="navbar-brand">Buggies</a>-->
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li>               <a href="logincontroller">Inicio sesión</a></li>
                    <li class="active"><a>Registro</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <div class="container theme-showcase" role="main">
        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
            <h1>Registro</h1>
            <p>Rellene todos los campos. La contraseña debe tener al menos 6 caracteres. No hace falta verificar el correo electrónico.</p>
        </div>
        <form class="form-horizontal" data-toggle="validator" role="form">
            <div class="row form-group has-feedback">
                <label for="inputName" class="control-label">Usuario</label>
                <input type="text" class="form-control" id="inputName" placeholder="Cliente" required>
                <div class="help-block with-errors"></div>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="row form-group has-feedback">
                <label for="inputEmail" class="control-label">Email</label>
                <input type="email" class="form-control" id="inputEmail" placeholder="Email" data-error="Email inválido" required>
                <div class="help-block with-errors"></div>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="row form-group has-feedback">
                <label class="control-label" for="inputPassword">Contraseña</label>
                <input type="password" data-minlength="6" class="form-control" id="inputPassword" data-error="Mínimo 6 caracteres" placeholder="Contraseña" required>
                <div class="help-block with-errors"></div>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="row form-group has-feedback">
                <label class="control-label" for="inputPasswordConfirm">Confirmar contraseña</label>
                <input type="password" class="form-control" name="confirmPassword" id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="No coinciden" placeholder="Confirmar" required/>
                <div class="help-block with-errors"></div>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            </div>
            <div class="row form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-1.12.2.min.js"></script>
        <script src="js/validator.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>

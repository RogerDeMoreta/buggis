<%@ page import="com.gmail.rogermoreta.buggis.SalidaEntity" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.gmail.rogermoreta.buggis.BuggiEntity" %>
<%--
  Created by IntelliJ IDEA.
  User: Roger
  Date: 03/04/2016
  Time: 22:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    Boolean usuarioIniciado = (Boolean) session.getAttribute("userInside") ;
    if (usuarioIniciado != null && usuarioIniciado) { //Usuario identificado
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Registro salida</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="css/theme.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="css/base.css" rel="stylesheet">
    <link href="css/prettify-1.0.css" rel="stylesheet">
    <link href="css/bootstrap-dialog.min.css" rel="stylesheet">

    <!-- icon -->
    <link rel="shortcut icon" href="img/buggy16.ico" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body role="document">
<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="pull-left">
                <img src="img/buggy512.png" style="height:50px; margin-left: 10px; margin-right: 15px">
            </a>
            <!--<a class="navbar-brand">Buggies</a>-->
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a>Salidas</a></li>
                <li>               <a href="buggies.jsp">Buggies</a></li>
                <li>               <a href="rutes.jsp">Rutas</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="logout">Salir</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<div class="container theme-showcase" role="main">
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <h1>Salidas</h1>
        <p>Consulta y añade salidas de los buggies</p>
    </div>
    <div class="accordion-heading">
        <button class="btn btn-default accordion-toggle"
                data-toggle="collapse"
                data-parent="#accordion2"
                href="#collapseOne">Registrar salida &raquo;</button>
    </div>
    <br>
    <div id="collapseOne" class="accordion-body collapse">
        <div class="accordion-toggle form-horizontal" data-toggle="validator" role="form">
            <div class="form-group">
                <label class="col-sm-3 col-xs-3 control-label">Seleccione la ruta</label>
                <div class="col-sm-4 col-xs-9 input-group">
                    <select class="form-control" id="rutaSelected">
                        <option disabled selected value> -- selecciona una ruta -- </option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                    </select>
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="col-sm-6 col-xs-11 control-label">Seleccione los buggies que hayan participado</label>
                <div class="col-sm-1 col-xs-1 input-group"></div>
            </div>
            <%
                Integer numBuggies = (Integer) session.getAttribute("numBuggies");
                if (numBuggies != null) {
                    ArrayList<BuggiEntity> buggies = (ArrayList<BuggiEntity>) session.getAttribute("buggies");
                    for (int i = 0; i < numBuggies; i++) {
                        BuggiEntity buggi = buggies.get(i);
                        if (buggi.getEstado() < 1) {%>
            <div id="buggyCheck<%=buggi.getId()%>" class="form-group">

                <label class="col-sm-3 col-xs-3 control-label"></label>
                <div class="input-group col-sm-4">
                    <span class="input-group-addon">
                        <input type="checkbox" aria-label="ha salido" value="<%=buggi.getId()%>" onchange="
                            this.checked ? $('#buggyCheck<%=buggi.getId()%>').addClass('has-success'):$('#buggyCheck<%=buggi.getId()%>').removeClass('has-success');
                        ">
                    </span>
                    <input type="text" class="form-control" readonly aria-label="<%=buggi.getMatricula()%>" value="<%=buggi.getMatricula()%>">
                </div><!-- /input-group -->
            </div>
            <%}}}%>
            <div class="form-group">
                <label class="col-sm-3 col-xs-3 control-label">Hora de la salida</label>
                <div class='col-sm-4 input-group date' id='datetimepicker1'>
                    <input id='mometoSalida' type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-xs-3 control-label">Comentarios</label>
                <div class="col-sm-4 col-xs-9 input-group">
                    <textarea id="comentario" class="form-control" rows="2" placeholder="(opcional)"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 col-xs-3 control-label"></label>
                <div class="input-group col-sm-4">
                    <button type="submit" id="botonSubmit" class="btn btn-primary">Registrar permanentemente</button>
                </div>
            </div>
        </div>
    </div>
    <br>

    <table class="table table-striped">
        <tr>
            <th>Nombre de la ruta</th>
            <th>Fecha salida</th>
            <th>Comentarios</th>
        </tr>
        <%
            Integer numSalidas = (Integer) session.getAttribute("numSalidas");
            ArrayList<SalidaEntity> salidas = (ArrayList<SalidaEntity>) session.getAttribute("salidas");
            if (numSalidas != null)
                for (int i = 0; i < numSalidas; i++) {
                    SalidaEntity salida = salidas.get(i);
        %>
        <tr>
            <td><%=salida.getName()%></td>
            <td><%=salida.getFechaSalida()%></td>
            <td><%=salida.getDescripcion()%></td>
        </tr>
        <%}%>
    </table>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.12.2.min.js"></script>
<script src="js/validator.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/moment-with-locales.js"></script>
<script src="js/bootstrap-datetimepicker.js"></script>
<script src="js/base.js"></script>
<script src="js/prettify-1.0.min.js"></script>
<script src="js/bootstrap-dialog.min.js"></script>
<!--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCOKP4CVXTp2cfqTwGKV0e7xNTHP-ykl1w&callback=initMap">
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -34.397, lng: 150.644},
        zoom: 8
    });
</script>-->
<script>
    $(document).ready(function() {
        $('#botonSubmit').click(function(event) {
            var rutaSelected = $('#rutaSelected').val();
            var buggies = [];
            $(':checkbox:checked').each(function(i){
                buggies[i] = $(this).val();
            });
            var tiempoSalida = $('#mometoSalida').val();
            var comentario = $('#comentario').val();


            ($(
                    '<div class="modal fade" id="pleaseWaitDialog" role="dialog" data-keyboard="false">' +
                    '<div class="modal-dialog">' +
                    '<!-- Modal content-->' +
                    '<div class="modal-content">' +
                    '<div class="modal-header">' +
                    '<h1>Processing...</h1>' +
                    '</div>' +
                    '<div class="modal-body">' +
                    '<div class="progress">' +
                    '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">' +
                    '<span class="sr-only">100% Complete</span>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>'
            )).modal();

            // Si en vez de por post lo queremos hacer por get, cambiamos el $.post por $.get
            $.post('salidascontroller', {
                    rutaSelected: rutaSelected,
                    'buggies[]': buggies,
                    tiempoSalida: tiempoSalida,
                    comentario: comentario
                },
                function(responseText) {
                    $("#pleaseWaitDialog").remove();
                    $(".modal-backdrop").remove();
                    if (responseText == "0") {
                        BootstrapDialog.show({
                            type: BootstrapDialog.TYPE_DANGER,
                            closable: false,
                            title: 'Error insertando salida',
                            message: 'Ha ocurrido algún error al insertar la salida en nuestras bases de datos.\nCompruebe' +
                            ' los datos introducidos. Si el problema persiste, inténtelo más tarde.',
                            buttons: [{
                                icon: 'glyphicon glyphicon-ban-circle',
                                label: 'Entendido',
                                cssClass: 'btn-danger',
                                action: function(dialogItself){
                                    dialogItself.close();
                                }
                            }]
                        });
                    } else if (responseText == "1") {
                        BootstrapDialog.show({
                            type: BootstrapDialog.TYPE_SUCCESS,
                            closable: false,
                            title: 'Inserción correcta',
                            message: 'Se ha insertado correctamente la nueva salida en nustras bases de datos.',
                            buttons: [{
                                icon: 'glyphicon glyphicon-ok',
                                cssClass: 'btn-success',
                                label: 'OK',
                                action: function(){
                                    window.location.href = "./introRecorrido.jsp";
                                }
                            }]
                        });
                    }
                }
            );
        });
    });

    $(function () {
        $('#datetimepicker1').datetimepicker({
            locale: 'es',
            defaultDate: new Date()
        });
    });
</script>
</body>
</html>
<%}else response.sendRedirect("logincontroller");%>